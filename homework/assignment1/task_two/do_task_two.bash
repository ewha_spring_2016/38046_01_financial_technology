# this script does task two of homework assignment one.

# NOTICE!
# It needs to be run in a clean VM 
# ToDo: Make it check to see if it needs to setup dependancies.

# First we get the files from GitHub repo : https://github.com/mnielsen/neural-networks-and-deep-learning

wget https://github.com/mnielsen/neural-networks-and-deep-learning/archive/master.zip

# Next we unzip the files.

unzip master.zip

# Make a directory for the task

mkdir hw_1_task_2

# Make a directory for the data

mkdir data

# Next we copy the files we need.

# This is the base of the command : cp -rf /source/path/ /destination/path/
# (the -rf is a switch with two options; recursive and force. It is very important that you be carefull with the use of the force switch!)

cp -rf neural-networks-and-deep-learning-master/src/* hw_1_task_2/

cp -rf neural-networks-and-deep-learning-master/data/* data/

# Let's make numpy work!

sudo apt-get update
sudo apt-get -y install python-numpy

cd hw_1_task_2
cat > task_two.py <<EOL
import mnist_loader
training_data, validation_data, test_data = mnist_loader.load_data_wrapper()

import network

net = network.Network([784, 30, 10])

net.SGD(training_data, 30, 10, 3.0, test_data=test_data)

EOL

python task_two.py
