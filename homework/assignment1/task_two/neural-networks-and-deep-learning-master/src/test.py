import mnist_loader

training_data, validation_data, test_data = mnist_loader.load_data_wrapper()

import network2

net = network2.Network([784, 30, 10], cost=network2.CrossEntropyCost)

net.large_weight_initializer()

# the 1000 is observations. (lmbda needs to increase as observations increase if the same effect is to be kept.)
net.SGD(training_data[:10000], 400, 10, 0.5, evaluation_data=test_data, lmbda = 1.0, monitor_evaluation_cost=True, monitor_evaluation_accuracy=True, monitor_training_cost=True, monitor_training_accuracy=True)

