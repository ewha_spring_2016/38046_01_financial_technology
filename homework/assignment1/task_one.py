# Benjamin Haos

import sys

# The next four line, not including the white space, define a simple function.

def say(str):
   # This prints the variable str that has been passed into this function
   print str
   return

# This function displays the arguements passed into the program.

def display_args_passed_into_the_program():
    if len(sys.argv) > 2:
        for index in range(len(sys.argv)):
            if index > 0:
                print 'Arguement %d : %s' % (index, sys.argv[index])
    else:
        print "There were no arguements passed into the program."
    return

# This is a function that abstracts the asking of yes or no questions

def ask_user_yes_or_no_question(question_to_ask):
    
    users_choice = raw_input(question_to_ask + " (y/n) : ")
    
    # ToDo : Handle not perfect syntax on user's input.
    while True:
        if users_choice == "y":
            return True
        elif users_choice == "n":
            return False
    return

# This function uses Python's built in open, read, and close functions to show the code of the program.

def show_the_code():
    
    # First we open the file and assign it to the variable file.
    file = open(sys.argv[0], 'r')
    
    # Then we read the file into the data variable.
    data = file.read()
    
    # Then display code.
    print "%s" % data
    
    #And finally, we close the file.
    file.close()
    
    return

def run_program():
    
    print ""
    print "Hello!\n"

    user_name = raw_input("What is your name? : ")

    print "\nHi %s, my name is %s" % (user_name, str(sys.argv[0]))
    
    print "I am a python program but some people think call me a script.\n"
    
    
    if sys.argv > 1:
        
        if ask_user_yes_or_no_question("Would you like to see the arguments?"):
            display_args_passed_into_the_program();
    
    else:
    
        print "Although i can handle arguments, your call was not argumentative!"
        print "When you call (run) the program, add some strings afterwards."

    if ask_user_yes_or_no_question("Would you like to see the code that makes me work?"):
    
        show_the_code();

    # This line calls function say and says goodbye.
    say("This is the end of the program.")
    
    return

run_program()
