## Ewha Womans' Financial Technology(2016 년도[year], 1학기[1st semester], 38046_01)

#### Taught by : [Doctor Seth Huang](http://biz.ewha.ac.kr/eng/esb/faculty/ba/prof?mode=list&page=7&major_id=11&sword=)

### HOMEWORK ASSIGNMENT ONE!
  * #### Due : April 5th 2016

Completed by : (IES15718) Benjamin Haos

* *Write a single script in Sublime that includes the following functions:*
    * *`argv`*
    * *`print`*
    * *`open/ read/ close`*
    * *`def`*
    * *`raw_input`*
    * *`comments`*
      * *REQUIRED* *The first line of your Python must include `# <your_name>, <your_student_id_number>`*

```PYTHON
# Benjamin Haos, IES15718

import sys

# The next four line, not including the white space, define a simple function.

def say(str):
   # This prints the variable str that has been passed into this function
   print str
   return

# This function displays the arguements passed into the program.

def display_args_passed_into_the_program():
    if len(sys.argv) > 2:
        for index in range(len(sys.argv)):
            if index > 0:
                print 'Arguement %d : %s' % (index, sys.argv[index])
    else:
        print "There were no arguements passed into the program."
    return

# This is a function that abstracts the asking of yes or no questions

def ask_user_yes_or_no_question(question_to_ask):
    
    users_choice = raw_input(question_to_ask + " (y/n) : ")
    
    # ToDo : Handle not perfect syntax on user's input.
    while True:
        if users_choice == "y":
            return True
        elif users_choice == "n":
            return False
    return

# This function uses Python's built in open, read, and close functions.
# It displays the code of the program.

def show_the_code():
    
    # First we open the file and assign it to the variable file.
    file = open(sys.argv[0], 'r')
    
    # Then we read the file into the data variable.
    data = file.read()
    
    # Then display code.
    print "%s" % data
    
    #And finally, we close the file.
    file.close()
    
    return

def run_program():
    
    print ""
    print "Hello!\n"

    user_name = raw_input("What is your name? : ")

    print "\nHi %s, my name is %s" % (user_name, str(sys.argv[0]))
    
    print "I am a python program but some people think call me a script.\n"
    
    
    if sys.argv > 1:
        
        if ask_user_yes_or_no_question("Would you like to see the arguments?"):
            display_args_passed_into_the_program();
    
    else:
    
        print "Although i can handle arguments, your call was not argumentative!"
        print "When you call (run) the program, add some strings afterwards."

    if ask_user_yes_or_no_question("Would you like to see the code that makes me work?"):
    
        show_the_code();

    # This line calls function say and says goodbye.
    say("This is the end of the program.")
    
    return

run_program()

```

* *Run the MNIST training explained in classwith the following changes:*
  * *Number of epochs = 30, 50, 100*
  * *Learning rate (eta) = 0.1, 2*

* ***Document** the results (xxxx/ 10000) for the last 20 epochs.*

This task was completed by running the following Python script:


```python
import mnist_loader
import sys

training_data, validation_data, test_data = mnist_loader.load_data_wrapper()

import network
net = network.Network([784, 30, 10])

if len(sys.argv) > 1:
    num_epochs = int(sys.argv[1])
else:
    num_epochs = 30

if len(sys.argv) > 2:
    learning_rate = float(sys.argv[2])
else:
    learning_rate = 3.0

print "\n * * * * * * * * * * * \n"

print "Running MNIST training with number of epochs set at %d and learning rate set at %f" % (num_epochs, learning_rate)

print "\n * * * * * * * * * * * \n"

net.SGD(training_data, num_epochs, 10, learning_rate, test_data=test_data)
```

Using these command line calls:

```bash
python task_two.py 30 0.1
python task_two.py 50 0.1
python task_two.py 100 0.1
python task_two.py 30 2.0
python task_two.py 50 2.0
python task_two.py 100 2.0
```

The entire task was automated with the following bash script:

```bash
# this script does task two of homework assignment one.

# NOTICE!
# It needs to be run in a clean VM 
# ToDo: Make it check to see if it needs to setup dependancies.

# First we get the files from GitHub repo : https://github.com/mnielsen/neural-networks-and-deep-learning

wget https://github.com/mnielsen/neural-networks-and-deep-learning/archive/master.zip

# Next we unzip the files.

unzip master.zip

# Make a directory for the task

mkdir hw_1_task_2

# Make a directory for the data

mkdir data

# Next we copy the files we need.

# This is the base of the command : cp -rf /source/path/ /destination/path/
# (the -rf is a switch with two options; recursive and force. It is very important that you be carefull with the use of the force switch!)

cp -rf neural-networks-and-deep-learning-master/src/* hw_1_task_2/

cp -rf neural-networks-and-deep-learning-master/data/* data/

# Let's make numpy work!

sudo apt-get update
sudo apt-get -y install python-numpy

cd hw_1_task_2
cat > task_two.py <<EOL
import mnist_loader
import sys

training_data, validation_data, test_data = mnist_loader.load_data_wrapper()

import network
net = network.Network([784, 30, 10])

if len(sys.argv) > 1:
    num_epochs = int(sys.argv[1])
else:
    num_epochs = 30

if len(sys.argv) > 2:
    learning_rate = float(sys.argv[2])
else:
    learning_rate = 3.0

print "\n * * * * * * * * * * * \n"

print "Running MNIST training with number of epochs set at %d and learning rate set at %f" % (num_epochs, learning_rate)

print "\n * * * * * * * * * * * \n"

net.SGD(training_data, num_epochs, 10, learning_rate, test_data=test_data)
EOL

python task_two.py 30 0.1
python task_two.py 50 0.1
python task_two.py 100 0.1
python task_two.py 30 2.0
python task_two.py 50 2.0
python task_two.py 100 2.0
```

The requested output is documented here :

```bash
 * * * * * * * * * * * 

Running MNIST training with number of epochs set at 30 and learning rate set at 0.100000

 * * * * * * * * * * * 

Epoch 10: 8844 / 10000
Epoch 11: 8879 / 10000
Epoch 12: 8902 / 10000
Epoch 13: 8948 / 10000
Epoch 14: 8945 / 10000
Epoch 15: 8970 / 10000
Epoch 16: 8995 / 10000
Epoch 17: 9024 / 10000
Epoch 18: 9043 / 10000
Epoch 19: 9068 / 10000
Epoch 20: 9081 / 10000
Epoch 21: 9101 / 10000
Epoch 22: 9108 / 10000
Epoch 23: 9126 / 10000
Epoch 24: 9132 / 10000
Epoch 25: 9140 / 10000
Epoch 26: 9151 / 10000
Epoch 27: 9158 / 10000
Epoch 28: 9170 / 10000
Epoch 29: 9184 / 10000

 * * * * * * * * * * * 

Running MNIST training with number of epochs set at 50 and learning rate set at 0.100000

 * * * * * * * * * * * 

Epoch 30: 9195 / 10000
Epoch 31: 9199 / 10000
Epoch 32: 9197 / 10000
Epoch 33: 9208 / 10000
Epoch 34: 9217 / 10000
Epoch 35: 9220 / 10000
Epoch 36: 9227 / 10000
Epoch 37: 9229 / 10000
Epoch 38: 9243 / 10000
Epoch 39: 9245 / 10000
Epoch 40: 9246 / 10000
Epoch 41: 9241 / 10000
Epoch 42: 9265 / 10000
Epoch 43: 9265 / 10000
Epoch 44: 9270 / 10000
Epoch 45: 9264 / 10000
Epoch 46: 9278 / 10000
Epoch 47: 9276 / 10000
Epoch 48: 9273 / 10000
Epoch 49: 9284 / 10000

 * * * * * * * * * * * 

Running MNIST training with number of epochs set at 100 and learning rate set at 0.100000

 * * * * * * * * * * * 

Epoch 80: 9320 / 10000
Epoch 81: 9317 / 10000
Epoch 82: 9322 / 10000
Epoch 83: 9333 / 10000
Epoch 84: 9325 / 10000
Epoch 85: 9330 / 10000
Epoch 86: 9325 / 10000
Epoch 87: 9334 / 10000
Epoch 88: 9335 / 10000
Epoch 89: 9337 / 10000
Epoch 90: 9336 / 10000
Epoch 91: 9335 / 10000
Epoch 92: 9343 / 10000
Epoch 93: 9344 / 10000
Epoch 94: 9347 / 10000
Epoch 95: 9337 / 10000
Epoch 96: 9342 / 10000
Epoch 97: 9348 / 10000
Epoch 98: 9343 / 10000
Epoch 99: 9351 / 10000

 * * * * * * * * * * * 

Running MNIST training with number of epochs set at 30 and learning rate set at 2.000000

 * * * * * * * * * * * 

Epoch 10: 8546 / 10000
Epoch 11: 8566 / 10000
Epoch 12: 8587 / 10000
Epoch 13: 8565 / 10000
Epoch 14: 8544 / 10000
Epoch 15: 8571 / 10000
Epoch 16: 8580 / 10000
Epoch 17: 8592 / 10000
Epoch 18: 8572 / 10000
Epoch 19: 8598 / 10000
Epoch 20: 8604 / 10000
Epoch 21: 8602 / 10000
Epoch 22: 9437 / 10000
Epoch 23: 9459 / 10000
Epoch 24: 9461 / 10000
Epoch 25: 9454 / 10000
Epoch 26: 9448 / 10000
Epoch 27: 9472 / 10000
Epoch 28: 9469 / 10000
Epoch 29: 9485 / 10000

 * * * * * * * * * * * 

Running MNIST training with number of epochs set at 50 and learning rate set at 2.000000

 * * * * * * * * * * * 

Epoch 30: 9505 / 10000
Epoch 31: 9496 / 10000
Epoch 32: 9493 / 10000
Epoch 33: 9471 / 10000
Epoch 34: 9477 / 10000
Epoch 35: 9497 / 10000
Epoch 36: 9499 / 10000
Epoch 37: 9505 / 10000
Epoch 38: 9503 / 10000
Epoch 39: 9508 / 10000
Epoch 40: 9504 / 10000
Epoch 41: 9491 / 10000
Epoch 42: 9502 / 10000
Epoch 43: 9470 / 10000
Epoch 44: 9500 / 10000
Epoch 45: 9510 / 10000
Epoch 46: 9497 / 10000
Epoch 47: 9494 / 10000
Epoch 48: 9489 / 10000
Epoch 49: 9488 / 10000

 * * * * * * * * * * * 

Running MNIST training with number of epochs set at 100 and learning rate set at 2.000000

 * * * * * * * * * * * 

Epoch 80: 9458 / 10000
Epoch 81: 9467 / 10000
Epoch 82: 9479 / 10000
Epoch 83: 9454 / 10000
Epoch 84: 9469 / 10000
Epoch 85: 9461 / 10000
Epoch 86: 9431 / 10000
Epoch 87: 9463 / 10000
Epoch 88: 9442 / 10000
Epoch 89: 9469 / 10000
Epoch 90: 9454 / 10000
Epoch 91: 9446 / 10000
Epoch 92: 9458 / 10000
Epoch 93: 9457 / 10000
Epoch 94: 9459 / 10000
Epoch 95: 9465 / 10000
Epoch 96: 9457 / 10000
Epoch 97: 9461 / 10000
Epoch 98: 9446 / 10000
Epoch 99: 9444 / 10000

```

* ToDo (in order of importance):
    * Have the output saved into file(s).
    * Mark each step with time stamp.
    * Figure out verification testing.
    * Increase error checking.
    * Record each step's resource use.

* *In 200 words, write down the basic concepts of:*
    * *The basic structure of an Artificial Neural Network.*
    * *Gradient Descent.*
    * *Cost Landscape.*

Artificial neural networks are computer-logic algorithms analogizing biological phenomena for purpose of locating, recording, and analyzing patterns found in non-standard data stores. Mirroring schemes found in our own brains, silicon-based nervous-systems successfully applied have completed seemingly simple task such as decoding hand writing and deciphering voice. An important, and very stunning algorithmic feature is their simplicity. In their most basic, preprocessed data is feeds a first of thee layered nodal system. This input layer, feeds a hidden second layer of all identical nodes that in turn feed a third output layer. It might be assumed a hidden layer obfuscates overly-complicated task-specific calculations. In fact, simple mathematics are employed, producing surprising accurate results. This feat of computerized magic is achieved through a process similar to that found in our own neural systems where first the system is fed training data and learns what bias to apply and which nodes to weight. Once educated, the system is able to discern answers on its own even with no specific instruction-set. The same system that can be applied to recognizing hand-writing could also be used to forecast the weather or predict market fluctuations.

Gradient descent, is a critically related concept for neural networks. Simply put, gradient descent is the push towards the minimization, and ideally elimination, of algorithm error. This error, often called cost, can be visualized into a two, three, four or even more, dimensioned landscape where troughs and peaks represent the hot or cold our learning machine can be thought to feel while it attempting to find the answer it seeks

* *In 400 words, write down what kind of data you can use for neural network and what would you like to model and predict? Describe the basic data structure and basic output structure.*

Being generously offered opportunity at critique of imagined application of just recent, and only sophomoric, understanding of concepts and methodologies for utilizing neural network to model and predict data; i must preface these statements with a caveat. Like an inquisitive child given a microscope or magnifying glass; i am no expert in these tool's utilization, risks or problems to consider, what to examine, nor what discoveries might be expected. Even so, i offer these thoughts with confidence that i will receive expert advice and hopes that advice will result in observation of perspectives previously hidden.

The most obvious choice is application directed towards financial data. Yahoo’s financial API could be scraped of data offering plenty analysis practice. Seeming obviosity is compounded by plethora of freely available financial data and records potentially process-able. Luckily, living enough years and making enough mistakes, i recognize  limitation. Even though this puzzle is intriguing, i know it to be a mountain, and i a fool if i pursue.

A data store i’ve a bit more experience with; a large hodgepodge of computer instruction code; text and image based documents; and audio and video files. Over the last few years i have generated a pile of files whose organizational scheme i’ve intentionally neglected so as to have a mess with which to apply a sorting system. Chaos given free reign, this mess' growth mirrored my my skill-set advancement, albeit a bit exponentially. Visions of machine cleaning of my metaphorical room are made more vivid with learning obtained in this class. through these files, after being trained on a set sorted previous, and returns simple information such as when these things were created, what they exemplify, and whether or not they should be reviewed further. More complicated knowledge i would like delivered would be what my interests are, how it is i learn, areas I’ve pursued that if given further effort would produce the greatest results, and perhaps how to control and manage this production of digital debris i seem to be creating at a rapidly increasing pace.

* Inputs would include:

    * Personally written or utilized code, libraries, modules, version-management histories, and other programmer type recordings.
    * Images, photos, illustrations, and graphics personally created and/or utilized.
    * Documents personally created, utilized, and/or archived for future enjoyment.
    * Browser, library, financial, location, and communication histories.

* Output uses would include:

    * HTML Document.
    * Guidance.
    * Portfolio/Resume.
