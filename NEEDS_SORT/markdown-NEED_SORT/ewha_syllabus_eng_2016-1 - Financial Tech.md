Syllabus (2016­1)
Course Title FINANCIAL TECHNOLOGY Course No. 38046
Department/
Major
Ewha School of Business Credit 3
Class Time/
Classroom
  Tue/ Thu: 3­3/ 2­2 (periods)
Instructor
  Seth H. Huang, PhD  Field of Finance
seth.huang@ewha.ac.kr +82­2­3277­5925
Office Hours/
Office Location
 Office location is ESB420
 Office hours: TBD
1. Course Description
This course focuses on the recent financial technology (Fintech) and deep learning. Fintech has been 
developed widely in the past few years. The foundation, machine learning and big data analytic, have 
applications in search engine, image understanding, apps, medicine and etc. We aim to provide a solid 
understanding in how deep learning works in finance and applies in the modern era and the potential 
applications. 
The foundation of these new applications can be attributed to deep neural network (aka “deep learning”) 
whose approaches have greatly advanced the performance of these state­of­the­art pattern recognition 
systems. This course discusses the background of the recent development, the mathematical foundation 
of deep neural network, with simple exercises based on Python. 
During the 15­week course, students will learn to learn basic coding in Python, gain an understanding in 
neural network and the recent developments in the financial industry with respect to the use of this 
methodology.  
This course seeks to provide a very basic foundation for programming as well as machine learning, both of
which are the building blocks of fintech and big data analytic.
2. Prerequisites
 Basic calculus. Linear Algebra is a plus
 Basic probability and statistics
 Basic knowledge in optimization and linear/non­linear modeling
 Basic understanding in programming
 Sufficient and basic understanding of English language, verbal communication as well as writing. 
3. Course Format
 │1
The course format consists of lectures and in­class activities. I will post on my Cyber Campus certain 
course materials, including notes, handouts, assignments/cases, and announcements. Please arrange to 
have access to the Cyber Campus.
Homework/project: Each assignment should be completed in teams. Write on each assignment paper: (a) 
class section; (b) assignment #; (c) full names
For details, please see Class Format and Grading Policy
Grading Policy
Grading Policy
The overall grade in this course will be determined based on the following elements:
4 assignments: 10%
Short Paper/ report: 10%
Presentation: 10%
Attendance: 10%
Midterm: 25%
Final Exam: 35%
Grade scale: The grades will be assigned in accordance to the policies set by Ewha School of Business.
4. Course Objectives
See Class Format
5. Evaluation System
 │2
Ⅰ. Course Overview
    The class will include textbook materials, cases and in­class activities. We will cover basic management 
principles as introduced in the textbook, and we will discuss relevant cases. 
Ⅱ. Course Materials and Additional Readings
1. Required Materials and Tools
Relevant materials will be posted on CyberCampus
ipython notebook/ python 2.7/ numpy package (web­based tool which will be explained in class)
2. Supplementary Materials
None.
3. Optional Additional Readings
None.
Ⅲ. Course Policies
There will be no extra homework, make­up exams or assignments except for verifiable medical­reasons 
or official school activities, which will be submitted to ESB for review. Job interviews or vacations will not 
be valid reasons for missing classes or assignments.  
Attendance sheet cannot be signed after 10 minutes into the class. 
Ⅳ. Course Schedule 
NOTE: The specific schedule is subject to change and designed under the assumption of 8 teams. The 
contents will not change.
Week Date
Week 1 03/01  Topics & Class Format  Class Introduction: Fintech and related buzzwords in recent 
years/ Basic command line/ terminal work
 │3
Week Date
Materials & Assignments  None
Week 2 03/08
Topics & Class Format
 Overview on deep learning and why it has been the focus in 
industry/ Basic Python structure
Materials & Assignments  Assignment 1
Week 3 03/15
Topics & Class Format  Neural Network Foundations Part 1
Materials & Assignments  None
Week 4 03/22
Topics & Class Format  Neural Network Foundations Part 2
Materials & Assignments  None
Week 5 03/29 
Topics & Class Format  Optimization 
Materials & Assignments  Assignment 2
Week 6 04/05 
Topics & Class Format  Big data analytic and finance applications
Materials & Assignments  None
Week 7 04/12 
Topics & Class Format
 Data versus Optimization Part 2
 Essence in Python
Materials & Assignments  None
Week 8 04/19
Topics & Class Format  Midterm Exam
Materials & Assignments  None
Week 9 04/26 
Topics & Class Format  Data versus Optimization Part 1
Materials & Assignments  Assignment 3
 │4
Week Date
Week
10
05/03
Topics & Class Format  Training on MNIST data
Materials & Assignments  None
Week
11
05/10 
Topics & Class Format
 The essence in financial data through training image recognition/
Machine learning applications in Fintech
Materials & Assignments  None
Week
12
05/17 
Topics & Class Format  Presentations
Materials & Assignments  Assignment 4
Week
13
05/24 
Topics & Class Format  Presentations
Materials & Assignments  None
Week
14
05/31
Topics & Class Format  TBD
Materials & Assignments  None
Week
15
06/07 
Topics & Class Format  Final Exam
Materials & Assignments  None
Makeup
Classes
(mm/d
d) /
(mm/d
d)
Topics & Class Format  N / A
Materials & Assignments  N / A
Ⅴ. Special Accommodations
* According to the University regulation #57, students with disabilities can request special accommodation related to
attendance, lectures, assignments, and/or tests by contacting the course professor at the beginning of semester. 
Based on the nature of the students’ requests, students can receive support for such accommodations from the 
course professor and/or from the Support Center for Students with Disabilities (SCSD).
 │5
* The contents of this syllabus are not final—they may be updated.
 │6