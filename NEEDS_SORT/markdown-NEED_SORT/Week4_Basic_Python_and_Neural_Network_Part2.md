NEURAL NETWORKS AND
PYTHON TUTORIALS PART 2
1.
2.
3.
http://yann.lecun.com/exdb/mnist/
• HTTPS://GITHUB.COM/MNIELSEN/NEURAL-NETWORKS-AND-DEEPLEARNING/ARCHIVE/MASTER.ZIP
1.
2.
3.
network.Network([784, 30, 10])
 784 dimensional input (28 x 28)
 30 neurons (in the hidden layer)
 10 outputs (0 – 9)
SEE IF YOU CAN GET THIS TO WORK
x1, x2, x3 are the inputs
w1, w2, w3 are the weights
0, 1 binary output
The neuron is a SIGNMOID
You can use the above model to answer various questions.
Ex: if the threshold is greater than 2, then the weather is
great, and if the threshold is lower than 2, everyone should
stay home.
As discussed last week, there are several layers.
Notice that b is just threshold moved to the other side, and it is
known as “bias” from here on.
w and x are now weight and input vectors (you should know what
this is) 
Suppose we are training the mnist dataset, where the inputs are
raw pixels. 
Suppose the network was mistakenly classifying an image as an "8"
when it should be a "9". We could figure out how to make a small
change in the weights and biases so the network gets a little closer
to classifying the image as a "9"
BUT a small change in the weights or bias of any single
perceptron in the network can sometimes cause the output of
that perceptron to completely flip, say from 0 to 1.
 We want the changes in weights and biases to HAVE SMALL
EFFECTS IN THE OUTPUTS!
 THIS CONCEPT IS OFTEN NOT EXPLAINED WELL, BUT IF YOU
THINK ABOUT THE PROBLEM THIS WAY, YOU UNDERSTAND ANY
ACTIVITATION FUNCTION CAN POTENTIALLY BE USED.
So, the input can take on any values. For example, greyscale can
lie between 0 to 255 (black to white). Instead of having outputs like
this,
We will have
And the sigma is “sigmoid function.”
What happens if bias or weight gets very large or very small?

The smoothness of the function means small changes in weights
and biases will result in small changes in the output.
Pay attention to the partial derivatives. What are they?
Also note that the changing output is a LINEAR function.

Desired (perfect) output:
The cost function is thus:
a is the output given by the model, which is a function of x, w, and
b, the input, weights and biases respectively.
Is just the length function for a vector v. 
First, the cost function is non-negative.
This cost function can be extended easily to incorporate different
techniques such as cross-entropy function, which we will learn later
on. 
What we are doing is to try and find the global minima. Note that
THE REAL COST FUNCTION IS NOT PRETTY LIKE THIS. 
We pick a random starting point for a ball, and suppose the ball
calculates the derivatives of the landscape. By studying these
derivatives, we can understand what the landscape looks like. 
Assuming v is direction the ball can move.
We are simply choosing the changing v (the directions) to make
changing C negative. Thus, the gradient vector of C (the rate of
change for C with respect to changing) is:
denotes the GRADIENT of a function.
This means the change in COST is in relation to:
Again, it is the change of the Cost function is in relation to the rate
of change of C times the change in v. v of course is our “weights”
and “biases” in the neural network.
Suppose we choose:
eta is the LEARNING RATE OF THE NETWORK. 
Based on last slide, we know that:
Since the learning rate is always positive, we are always changing in
a direction that the COST WILL DECREASE!
So, essentially, we keep changing at the learning rate UNTIL WE
REACH GLOBAL MINIMA. How big should the learning rate be then?
What happens if it is too small? What happens if it is too big?
Remember, the whole idea is that we are finding weights and
biases to minimize the cost.
Now, for every single neuron in the hidden layer:
We know that we are simply changing the weights and biases so
that when input x goes through the node, the output will be as close
to our label as possible. Right?

Now, with bias, for every single neuron in the hidden layer:


The input pixels are greyscaled. Since you will likely use this structure
for SOMETHING ELSE, we will just explain the inputs brieftly.
The value of 1.0 => the pixel is white
The value of 0.9 => the pixel is black
Hidden layer has 15 neurons (a small selection)
Output has 10 neurons. If the first neuron has output of 1, then it
means the network is SURE that the input image is a “1”

More tutorials:
On line 1 we have what's called an "import." This is how you add features to your script from the Python feature set.
Rather than give you all the features at once, Python asks you to say what you plan to use. This keeps your programs
small, but it also acts as documentation for other programmers who read your code later.
The argv is the "argument variable," a very standard name in programming, that you will find used in many other
languages. This variable holds the arguments you pass to your Python script when you run it. In the exercises you will
get to play with this more and see what happens.
Line 3 "unpacks" argv so that, rather than holding all the arguments, it gets assigned to four variables you can work
with: script, first, second, and third. This may look strange, but "unpack" is probably the best word to describe what it does.
It just says, "Take whatever is in argv, unpack it, and assign it to all of these variables on the left in order."

Save a text file somewhere (in the same folder as your codes),
“this is some random stuff I am typing into the file”




