:<<EOF

SCRIPT_OUTPUT_DIRECTORY="./haos_work/fintech_task_runners/data_runners_created"

if [ -d "$SCRIPT_OUTPUT_DIRECTORY" ]
then
    echo "Found : $SCRIPT_OUTPUT_DIRECTORY"
else
    echo "Creating : $SCRIPT_OUTPUT_DIRECTORY"
    mkdir "$SCRIPT_OUTPUT_DIRECTORY"
fi

python haos_work/fintech_task_runners/fintec_test_script.py > $SCRIPT_OUTPUT_DIRECTORY/fintec_test_script_output.txt

EOF

# python haos_work/fintech_task_runners/example_script_2.py
# python haos_work/fintech_task_runners/example_script_3.py
# python haos_work/fintech_task_runners/example_script_4.py
# python haos_work/fintech_task_runners/example_script_5.py
python haos_work/fintech_task_runners/example_script_6.py


