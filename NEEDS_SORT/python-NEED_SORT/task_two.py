import mnist_loader
import sys

training_data, validation_data, test_data = mnist_loader.load_data_wrapper()

import network
net = network.Network([784, 30, 10])

if len(sys.argv) > 1:
    num_epochs = int(sys.argv[1])
else:
    num_epochs = 30

if len(sys.argv) > 2:
    learning_rate = float(sys.argv[2])
else:
    learning_rate = 3.0

print "\n * * * * * * * * * * * \n"

print "Running MNIST training with number of epochs set at %d and learning rate set at %f" % (num_epochs, learning_rate)

print "\n * * * * * * * * * * * \n"

net.SGD(training_data, num_epochs, 10, learning_rate, test_data=test_data)
