SCRIPT_OUTPUT_DIRECTORY="./haos_work/fintech_task_runners/data_runners_created"

if [ -d "$SCRIPT_OUTPUT_DIRECTORY" ]
then
    echo "Found : $SCRIPT_OUTPUT_DIRECTORY"
else
    echo "Creating : $SCRIPT_OUTPUT_DIRECTORY"
    mkdir "$SCRIPT_OUTPUT_DIRECTORY"
fi

#python haos_work/fintech_task_runners/example_script_1.py
python haos_work/student_drinking_scripts/student_drinking_test_2.py
